import java.awt.*;
import java.awt.event.*;
import java.awt.GridLayout;

import javax.swing.*;


public class Pizza {
	private JFrame mainFrame;
	private JLabel statusLabel;
    private JPanel controlPanel;
    private JTextField txtSpecialRequests;
    private JTextField txtToppings;
    private String[] toppingArray;
    
    
    
    public Pizza(){
    	PizzaGUI();
    	
    }
    /**
     * @wbp.parser.entryPoint
     */
    private void PizzaGUI(){
    	Integer[] size1 = {5,10,12,14};
    	Integer[] Qantity1 = {1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20};
		JComboBox size = new JComboBox(size1);
		JComboBox Qantity = new JComboBox(Qantity1);
		size.setBounds(123, 66, 61, 35);
		Qantity.setBounds(123, 126, 61, 35);
		mainFrame = new JFrame("Add Pizza");
		mainFrame.setSize(361,510);
		mainFrame.getContentPane().setLayout(null);
		mainFrame.getContentPane().add(size);
		mainFrame.getContentPane().add(Qantity);
		
		
		JLabel lblPizza = new JLabel("Pizza");
		lblPizza.setHorizontalAlignment(SwingConstants.CENTER);
		lblPizza.setFont(new Font("Tahoma", Font.PLAIN, 26));
		lblPizza.setBounds(107, 16, 118, 26);
		mainFrame.getContentPane().add(lblPizza);
		
		JLabel lblSize = new JLabel("Size");
		lblSize.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblSize.setBounds(29, 73, 69, 20);
		mainFrame.getContentPane().add(lblSize);
		
		JLabel lblQuantitiy = new JLabel("Quantitiy");
		lblQuantitiy.setFont(new Font("Tahoma", Font.PLAIN, 22));
		lblQuantitiy.setBounds(15, 133, 93, 28);
		mainFrame.getContentPane().add(lblQuantitiy);
		
		txtSpecialRequests = new JTextField();
		txtSpecialRequests.setHorizontalAlignment(SwingConstants.CENTER);
		txtSpecialRequests.setBounds(29, 355, 243, 26);
		mainFrame.getContentPane().add(txtSpecialRequests);
		txtSpecialRequests.setColumns(10);
		
		txtToppings = new JTextField();
		txtToppings.setHorizontalAlignment(SwingConstants.CENTER);
		txtToppings.setBounds(29, 273, 243, 26);
		mainFrame.getContentPane().add(txtToppings);
		txtToppings.setColumns(10);
		
		JLabel lblToppings = new JLabel("Toppings:");
		lblToppings.setHorizontalAlignment(SwingConstants.CENTER);
		lblToppings.setBounds(96, 238, 110, 20);
		mainFrame.getContentPane().add(lblToppings);
		
		JLabel lblSpecialRequests = new JLabel("Special Requests:");
		lblSpecialRequests.setBounds(93, 315, 132, 20);
		mainFrame.getContentPane().add(lblSpecialRequests);
		
		JLabel lblSpecial = new JLabel("Special:");
		lblSpecial.setBounds(203, 66, 69, 20);
		mainFrame.getContentPane().add(lblSpecial);
		
		JCheckBox chckbxBbq = new JCheckBox("BBQ");
		chckbxBbq.setBounds(189, 87, 139, 29);
		mainFrame.getContentPane().add(chckbxBbq);
		
		JCheckBox chckbxAlfrado = new JCheckBox("Alfrado");
		chckbxAlfrado.setBounds(189, 113, 139, 29);
		mainFrame.getContentPane().add(chckbxAlfrado);
		
		JCheckBox chckbxBacon = new JCheckBox("Bacon Ranch");
		chckbxBacon.setBounds(189, 146, 139, 29);
		mainFrame.getContentPane().add(chckbxBacon);
		
		JButton btnAdd = new JButton("Add");
		btnAdd.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				//todo when submit button is pressed
			}
		});
		btnAdd.setBounds(15, 395, 115, 28);
		mainFrame.getContentPane().add(btnAdd);
		
		JButton btnCancel = new JButton("Cancel");
		btnCancel.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				//todo when cancel button is pressed
			}
		});
		btnCancel.setBounds(156, 397, 115, 26);
		mainFrame.getContentPane().add(btnCancel);
		
		mainFrame.setVisible(true);
    	
    }
}
