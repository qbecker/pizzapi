import java.awt.*;
import java.awt.event.*;
import java.awt.GridLayout;
import javax.swing.*;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;


public class MainMenu {
	private JFrame mainFrame;
	private JLabel statusLabel;
    private JPanel controlPanel;
	
	/**
	 * @wbp.parser.entryPoint
	 */
	public MainMenu(){
		MainMenuGUI();
	}
	
	public static void main(String args[]){
		MainMenu MainMenu = new MainMenu();
		//MainMenu.showEventDemo();
		//Pizza pizza = new Pizza();
	}
	
	private void MainMenuGUI(){
		String[] items1 = {"Pizza", "Sandwich", "Natchos", "Drinks", "Alchol"};
		JComboBox item = new JComboBox(items1);
		//generate new window based on menue item selected
		item.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				if(item.getSelectedItem() == "Pizza"){
					Pizza pizza = new Pizza();
				}
			}
		});
		item.setModel(new DefaultComboBoxModel(new String[] {"Pizza", "Sandwich", "Natchos", "Drinks", "Alchol", "poop"}));
		item.setBounds(179, 117, 106, 27);
		mainFrame = new JFrame("Main Dine in Menu");
		mainFrame.setSize(500,800);
		mainFrame.addWindowListener(new WindowAdapter(){
			public void windowClosing(WindowEvent windowEvent){
				System.exit(0);
			}
		});
		
		controlPanel = new JPanel();
		controlPanel.setBounds(0, 659, 484, 76);
		controlPanel.setLayout(new FlowLayout());
		mainFrame.getContentPane().setLayout(null);
	    mainFrame.getContentPane().add(controlPanel);
	    mainFrame.getContentPane().add(item);
	    
	    JLabel lblNewOrder = new JLabel("New order");
	    lblNewOrder.setFont(new Font("Tahoma", Font.PLAIN, 24));
	    lblNewOrder.setBounds(153, 39, 224, 34);
	    mainFrame.getContentPane().add(lblNewOrder);
	    statusLabel = new JLabel("",JLabel.CENTER);
	    statusLabel.setBounds(10, 273, 446, 406);
	    mainFrame.getContentPane().add(statusLabel);
	    
	    JLabel lblItem = new JLabel("Item");
	    lblItem.setFont(new Font("Tahoma", Font.PLAIN, 22));
	    lblItem.setBounds(88, 105, 113, 45);
	    mainFrame.getContentPane().add(lblItem);
	    
	    JLabel lblDrinks = new JLabel("# Drinks");
	    lblDrinks.setFont(new Font("Tahoma", Font.PLAIN, 22));
	    lblDrinks.setBounds(81, 172, 84, 55);
	    mainFrame.getContentPane().add(lblDrinks);
	    
	    JComboBox comboBox = new JComboBox();
	    comboBox.setModel(new DefaultComboBoxModel(new String[] {"1", "2", "3", "4", "5", "6", "7", "8", "9", "10", "11", "12", "13", "14", "15", "16", "17", "18", "19", "20"}));
	    comboBox.setBounds(175, 194, 51, 20);
	    mainFrame.getContentPane().add(comboBox);
	    
	    JLabel lblCurrentOrder = new JLabel("Current Order");
	    lblCurrentOrder.setBounds(178, 242, 142, 20);
	    mainFrame.getContentPane().add(lblCurrentOrder);
	    
	    JCheckBox chckbxCarryOut = new JCheckBox("Carry out");
	    chckbxCarryOut.setBounds(303, 46, 139, 29);
	    mainFrame.getContentPane().add(chckbxCarryOut);
	    mainFrame.setVisible(true);
	    
		
		
		}
	 private void showEventDemo(){

	      JButton okButton = new JButton("Select");
	      JButton cancelButton = new JButton("Cancel");

	      okButton.setActionCommand("Okay\r\n");
	      cancelButton.setActionCommand("Cancel");

	      okButton.addActionListener(new ButtonClickListener()); 
	      cancelButton.addActionListener(new ButtonClickListener()); 

	      controlPanel.add(okButton);
	      JButton submitButton = new JButton("Submit");
	      submitButton.setActionCommand("Submit");
	      submitButton.addActionListener(new ButtonClickListener()); 
	      controlPanel.add(submitButton);
	      controlPanel.add(cancelButton);       

	      mainFrame.setVisible(true);  
	   }
	 private class ButtonClickListener implements ActionListener{
	      public void actionPerformed(ActionEvent e) {
	         String command = e.getActionCommand();  
	         if( command.equals( "OK" ))  {
	         }
	         else if( command.equals( "Submit" ) )  {
	            statusLabel.setText("Submit Button clicked."); 
	         }
	         else  {
	            statusLabel.setText("Cancel Button clicked.");
		
	}
	      }
	 }
}